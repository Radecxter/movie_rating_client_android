package com.example.gdnic.movieratingclient;

import android.app.Person;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MovieActivity extends AppCompatActivity {

    TextView titleTextView, descriptionTextView, genderTextView, directorTextView, yearTextView, minsTextView;
    Button enviar;
    Button edit, deletar;
    String movieId;
    Movie movie;
    ArrayList<String> comments = new ArrayList<>();
    ListView movieComments;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);



        titleTextView = findViewById(R.id.titleTextView);
        descriptionTextView = findViewById(R.id.descriptionTextView);
        genderTextView = findViewById(R.id.genderTextView);
        directorTextView = findViewById(R.id.directorTextView);
        yearTextView = findViewById(R.id.yearTextView);
        minsTextView = findViewById(R.id.minsTextView);

        movieComments = findViewById(R.id.movieComments);

        enviar = findViewById(R.id.enviar);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText comentario = findViewById(R.id.editTextComentario);
                createComment(comentario.getText().toString());
            }
        });

        Intent i = getIntent();
        this.user = (User) i.getSerializableExtra("user");

        getMovie(i.getStringExtra("id"));
        edit = findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditActivity.class);
                intent.putExtra("movie", movie);
                intent.putExtra("user", user);
                startActivity(intent);
            }
        });

        deletar = findViewById(R.id.deletar);
        deletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteContent deleteContent = new DeleteContent(movie);
                deleteContent.execute();
            }
        });
    }

    public void setListView() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, comments);
        movieComments.setAdapter(arrayAdapter);
    }

    public class GetContent extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();

                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }

                Log.i("JSON", result);

                return result;

            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Error", e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String movieJSON) {
            super.onPostExecute(movieJSON);



            ObjectMapper mapper = new ObjectMapper();
            try {
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                movie = mapper.readValue(movieJSON, Movie.class);
                titleTextView.setText(movie.getName());
                descriptionTextView.setText("Sinopse: " + movie.getDescription());
                genderTextView.setText("Gênero: " + movie.getGender());
                directorTextView.setText("Diretor: " + movie.getDirector());
                yearTextView.setText("Ano de lançamento: " + movie.getYear());
                minsTextView.setText("Duração: " + movie.getMins() + " Minutos");

                for(Comment comment: movie.getComments()){
                   comments.add("Adicionado por: " + comment.user.getUsername() + "\n Comentario: " + comment.commentary);
                }

                setListView();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void getMovie(String id) {
        try {
            GetContent task = new GetContent();
            task.execute(Properties.SERVIDOR + "/movies/" + id + ".json");
        } catch (Exception e) {
            Log.i("Erro", e.getMessage());
            e.printStackTrace();
        }
    }

    public class PostContent extends AsyncTask<String, Void, Boolean> {

        Comment comment;

        public PostContent() {}

        public PostContent(Comment comment) {
            this.comment = comment;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO: attempt authentication against a network service.

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(Properties.SERVIDOR + "/comments");
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept","application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);

                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));

                ObjectMapper mapper = new ObjectMapper();
                String jsonStr = mapper.writeValueAsString(comment);

                Log.i("Mamao", jsonStr);

                writer.write(jsonStr);
                writer.flush();
                writer.close();
                out.close();

                urlConnection.connect();

                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();

                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }

                reader.close();
                in.close();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                comment = mapper.readValue(result, Comment.class);

            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Error", e.getMessage());
                return false;
            }

            if(comment.getId() == null)
                return false;
            return true;
        }

        @Override
        protected void onPostExecute(Boolean deuCerto) {
            comments.add("Adicionado por: " + comment.user.getUsername() + "\n Comentario: " + comment.commentary);
            EditText comentario = findViewById(R.id.editTextComentario);
            comentario.setText("");
            setListView();
        }
    }

    public class DeleteContent extends AsyncTask<String, Void, Boolean> {

        Movie movie;

        public DeleteContent() {}

        public DeleteContent(Movie movie) {
            this.movie = movie;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO: attempt authentication against a network service.

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(Properties.SERVIDOR + "/movies/" + movie.id);
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("DELETE");
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setDoOutput(true);
                urlConnection.getResponseCode();

            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Error", e.getMessage());
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean deuCerto) {
            Intent intent = new Intent(getApplicationContext(), MovieRatingClient.class);
            intent.putExtra("user", user);
            startActivity(intent);
        }
    }

    public void createComment(String commentary){
        Comment comment = new Comment(commentary, user, movie);
        PostContent postContent = new PostContent(comment);
        postContent.execute(Properties.SERVIDOR + "/comments");
    }

}
