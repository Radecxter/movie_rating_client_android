package com.example.gdnic.movieratingclient;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Comment implements Serializable {
    @JsonProperty("id")
    String id;
    @JsonProperty("commentary")
    String commentary;
    @JsonProperty("user")
    User user;
    @JsonProperty("movie")
    Movie movie;

    public Comment() {
    }

    public Comment(String commentary, User user, Movie movie) {
        this.commentary = commentary;
        this.user = user;
        this.movie = movie;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
