package com.example.gdnic.movieratingclient;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Movie implements Serializable{

    @JsonProperty("id")
    String id;
    @JsonProperty("name")
    String name;
    @JsonProperty("description")
    String description;
    @JsonProperty("gender")
    String gender;
    @JsonProperty("director")
    String director;
    @JsonProperty("year")
    String year;
    @JsonProperty("minutes")
    String mins;
    @JsonProperty("rates_average")
    float ratesAVG;
    @JsonProperty("comments")
    ArrayList<Comment> comments;

    public Movie() {
    }

    public Movie(String id, String name, String description, String gender, String director, String year, String mins, float ratesAVG, ArrayList<Comment> comments) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.gender = gender;
        this.director = director;
        this.year = year;
        this.mins = mins;
        this.ratesAVG = ratesAVG;
        this.comments = comments;
    }

    public Movie(String id, String name, String description, String gender, String director, String year, String mins) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.gender = gender;
        this.director = director;
        this.year = year;
        this.mins = mins;
    }

    public Movie(String name, String description, String gender, String director, String year, String mins) {
        this.name = name;
        this.description = description;
        this.gender = gender;
        this.director = director;
        this.year = year;
        this.mins = mins;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMins() {
        return mins;
    }

    public void setMins(String mins) {
        this.mins = mins;
    }

    public float getRatesAVG() {
        return ratesAVG;
    }

    public void setRatesAVG(float ratesAVG) {
        this.ratesAVG = ratesAVG;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", gender='" + gender + '\'' +
                ", director='" + director + '\'' +
                ", year='" + year + '\'' +
                ", mins='" + mins + '\'' +
                ", ratesAVG=" + ratesAVG +
                ", comments=" + comments +
                '}';
    }
}
