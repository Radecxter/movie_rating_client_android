package com.example.gdnic.movieratingclient;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class EditActivity extends AppCompatActivity {

    Movie movie;
    User user;

    EditText title, description, director, year, minutes, gender;

    Button atualizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        title = findViewById(R.id.editTextTitle);
        description = findViewById(R.id.editTextDescription);
        director = findViewById(R.id.editTextDirector);
        year = findViewById(R.id.editTextYear);
        minutes = findViewById(R.id.editTextMinutes);
        gender = findViewById(R.id.editTextGender);
        atualizar = findViewById(R.id.buttonUpdate);


        Intent i = getIntent();
        this.movie = (Movie) i.getSerializableExtra("movie");
        this.user = (User) i.getSerializableExtra("user");

        title.setText(movie.getName());
        description.setText(movie.getDescription());
        director.setText(movie.getDirector());
        year.setText(movie.getYear());
        minutes.setText(movie.getMins());
        gender.setText(movie.getGender());

        atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameStr = title.getText().toString();
                String descriptionStr = description.getText().toString();
                String directorStr = director.getText().toString();
                String yearStr = year.getText().toString();
                String minutesStr = minutes.getText().toString();
                String genderStr = gender.getText().toString();
                Movie editedMovie = new Movie(movie.getId(), nameStr, descriptionStr, genderStr, directorStr, yearStr, minutesStr);

                PutContent putContent = new PutContent(editedMovie);
                putContent.execute(Properties.SERVIDOR + "/comments");
            }
        });
    }

    public class PutContent extends AsyncTask<String, Void, Boolean> {

        Movie movie;

        public PutContent() {}

        public PutContent(Movie movie) {
            this.movie = movie;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO: attempt authentication against a network service.

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(Properties.SERVIDOR + "/movies/" + movie.id);
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("PUT");
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept","application/json");
                urlConnection.setDoOutput(true);

                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));

                ObjectMapper mapper = new ObjectMapper();
                String jsonStr = mapper.writeValueAsString(movie);

                Log.i("Mamao", jsonStr);

                writer.write(jsonStr);
                writer.flush();
                writer.close();
                out.close();

                urlConnection.getInputStream();

            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Error", e.getMessage());
                return false;
            }

            if(movie.getId() == null)
                return false;
            return true;
        }

        @Override
        protected void onPostExecute(Boolean deuCerto) {
            Intent intent = new Intent(getApplicationContext(), MovieRatingClient.class);
            intent.putExtra("user", user);
            startActivity(intent);
        }
    }
}
