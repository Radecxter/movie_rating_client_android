package com.example.gdnic.movieratingclient;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MovieRatingClient extends AppCompatActivity {

    ArrayList<Movie> movies = new ArrayList<>();
    ListView moviesList;
    User user;

    Button adicionar;

    public void setListView() {
        ArrayList<String> moviesTitle = new ArrayList<>();

        for (int i = 0; i < movies.size(); i++) {
            moviesTitle.add(movies.get(i).name);
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, moviesTitle);
        moviesList.setAdapter(arrayAdapter);

        moviesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), MovieActivity.class);
                intent.putExtra("id", movies.get(position).id);
                intent.putExtra("user", user);
                startActivity(intent);
            }
        });

        adicionar = findViewById(R.id.buttonAdicionar);
        adicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewActivity.class);
                intent.putExtra("user", user);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_rating_client);
        moviesList = findViewById(R.id.ListView);
        Intent i = getIntent();
        this.user = (User) i.getSerializableExtra("user");
        getMovies();
    }

    public class GetContent extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();

                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }

                Log.i("JSON", result);

                reader.close();
                in.close();
                return result;

            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Error", e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(String moviesJSON) {
            super.onPostExecute(moviesJSON);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            try {
                ArrayList<Movie> moviesList = mapper.readValue(moviesJSON, new TypeReference<ArrayList<Movie>>(){});
                for(Movie movie : moviesList){
                    movies.add(movie);
                }

                setListView();

            } catch (JsonParseException e) {
                Log.e("Tanga", e.getMessage());
            } catch (JsonMappingException e) {
                Log.e("Tanga", e.getMessage());
            } catch (IOException e) {
                Log.e("Tanga", e.getMessage());
            }
        }
    }

    public void getMovies() {
        try {
            GetContent task = new GetContent();
            task.execute( Properties.SERVIDOR + "/movies.json");
        } catch (Exception e) {
            Log.e("Tanga", e.getMessage());
            e.printStackTrace();
        }
    }
}
