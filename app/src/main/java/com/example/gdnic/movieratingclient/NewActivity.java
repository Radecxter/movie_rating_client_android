package com.example.gdnic.movieratingclient;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class NewActivity extends AppCompatActivity {

    EditText title, description, director, year, minutes, gender;
    Button criar;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        title = findViewById(R.id.editTextNameNew);
        description = findViewById(R.id.editTextDescriptionNew);
        director = findViewById(R.id.editTextDirectorNew);
        year = findViewById(R.id.editTextYearNew);
        minutes = findViewById(R.id.editTextMinutesNew);
        gender = findViewById(R.id.editTextGenderNew);
        criar = findViewById(R.id.buttonCriar);

        Intent i = getIntent();
        this.user = (User) i.getSerializableExtra("user");

        criar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameStr = title.getText().toString();
                String descriptionStr = description.getText().toString();
                String directorStr = director.getText().toString();
                String yearStr = year.getText().toString();
                String minutesStr = minutes.getText().toString();
                String genderStr = gender.getText().toString();

                Movie newMovie = new Movie(nameStr, descriptionStr, genderStr, directorStr, yearStr, minutesStr);
                PostContent postContent = new PostContent(newMovie);
                postContent.execute();
            }
        });
    }

    public class PostContent extends AsyncTask<String, Void, Boolean> {

        Movie movie;

        public PostContent() {}

        public PostContent(Movie movie) {
            this.movie = movie;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            // TODO: attempt authentication against a network service.

            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(Properties.SERVIDOR + "/movies");
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                urlConnection.setRequestProperty("Accept","application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);

                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));

                ObjectMapper mapper = new ObjectMapper();
                String jsonStr = mapper.writeValueAsString(movie);

                Log.i("Mamao", jsonStr);

                writer.write(jsonStr);
                writer.flush();
                writer.close();
                out.close();

                urlConnection.connect();

                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();

                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }

                reader.close();
                in.close();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                movie = mapper.readValue(result, Movie.class);

            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Error", e.getMessage());
                return false;
            }

            if(movie.getId() == null)
                return false;
            return true;
        }

        @Override
        protected void onPostExecute(Boolean deuCerto) {
            Intent intent = new Intent(getApplicationContext(), MovieRatingClient.class);
            intent.putExtra("user", user);
            startActivity(intent);
        }
    }

}
